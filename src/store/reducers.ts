import { reducer as weatherReducer } from '../Features/Weather/reducer';
import {reducer as metricsReducer} from "../Features/Metrics/reducer";
import {reducer as multipleMeasurementsReducer} from "../Features/MultipleMeasurements/reducer";

export default {
  weather: weatherReducer,
  metrics: metricsReducer,
  multipleMeasurements: multipleMeasurementsReducer
};
