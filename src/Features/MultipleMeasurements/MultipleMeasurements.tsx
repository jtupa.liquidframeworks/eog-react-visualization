import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from './reducer';
import { Provider, createClient, useQuery } from 'urql';
import { useGeolocation } from 'react-use';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '../../components/Chip';
import { IState } from '../../store';

const client = createClient({
  url: 'https://react.eogresources.com/graphql',
});

const query = `
query($input: [MeasurementQuery]) {
  getMultipleMeasurements(input: $input) {
    metric
    measurements {
      at
      value
      metric
      unit
      __typename
    }
    __typename
  }
  __typename
}
`;

const getMultipleMeasurements = (state: IState) => {
  const { metric, measurements, __typename } = state.multipleMeasurements;
  return {
    metric,
    measurements,
    __typename,
  };
};

export default () => {
  return (
    <Provider value={client}>
      <MultipleMeasurements />
    </Provider>
  );
};

const MultipleMeasurements = () => {
  const getLocation = useGeolocation();
  // Default to oilTemp
  const input = {"metricName": "oilTemp"};
  const dispatch = useDispatch();
  const { metric, measurements, __typename } = useSelector(getMultipleMeasurements);

  const [result] = useQuery({
    query,
    variables: {
      input,
    },
  });
  const { fetching, data, error } = result;
  useEffect(() => {
    if (error) {
      dispatch(actions.multipleMeasurementsApiErrorReceived({ error: error.message }));
      return;
    }
    if (!data) return;
    const { getMultipleMeasurements } = data;
    dispatch(actions.multipleMeasurementsDataRecevied(getMultipleMeasurements));
  }, [dispatch, data, error]);

  if (fetching) return <LinearProgress />;

  return <Chip label={`MultipleMeasurements in ${ metric }: ${measurements} and ${__typename}°`} />;
};
