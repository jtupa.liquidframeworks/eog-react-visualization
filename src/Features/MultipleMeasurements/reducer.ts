import { createSlice, PayloadAction } from 'redux-starter-kit';

export type MultipleMeasurements = {
  metric: string;
  measurements: object;
  __typename: string;
};

export type ApiErrorAction = {
  error: string;
};

const initialState = {
  metric: '',
  measurements: {},
    __typename:'',
};


const slice = createSlice({
  name: 'multipleMeasurements',
  initialState,
  reducers: {
    multipleMeasurementsDataRecevied: (state, action: PayloadAction<MultipleMeasurements>) => {
      const { metric, measurements, __typename } = action.payload;
      state.metric = metric;
      state.measurements = measurements;
      state.__typename = __typename;
    },
    multipleMeasurementsApiErrorReceived: (state, action: PayloadAction<ApiErrorAction>) => state,
  },
});

export const reducer = slice.reducer;
export const actions = slice.actions;
